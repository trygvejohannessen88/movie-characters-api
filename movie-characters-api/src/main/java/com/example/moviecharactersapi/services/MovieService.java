package com.example.moviecharactersapi.services;

import com.example.moviecharactersapi.models.Movie;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public interface MovieService extends CrudService<Movie,Integer> {
    void updateCharacters(Collection<Integer> characterIds, int id);
}
