package com.example.moviecharactersapi.services;

import com.example.moviecharactersapi.exceptions.ResourceNotFoundException;
import com.example.moviecharactersapi.models.Movie;
import com.example.moviecharactersapi.repositories.CharacterRepository;
import com.example.moviecharactersapi.repositories.MovieRepository;
import org.springframework.stereotype.Service;
import java.util.Collection;

@Service
public class MovieServiceImp implements MovieService {
    private final MovieRepository movieRepository;
    private final CharacterRepository characterRepository;

    public MovieServiceImp(MovieRepository movieRepository, CharacterRepository characterRepository) {
        this.movieRepository = movieRepository;
        this.characterRepository = characterRepository;
    }

    @Override
    public Movie findById(Integer id) {
        return movieRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Movie With the Id: " + id + " Could not be found"));
    }

    @Override
    public Collection<Movie> findAll() {
        return movieRepository.findAll();
    }

    @Override
    public Movie add(Movie entity) {
        return movieRepository.save(entity);
    }

    @Override
    public Movie update(Movie entity) {
        return movieRepository.save(entity);
    }

    @Override
    public void deleteById(Integer integer) {
        movieRepository.deleteById(integer);
    }

    @Override
    public void delete(Movie entity) {
        movieRepository.delete(entity);
    }

    @Override
    public void updateCharacters(Collection<Integer> characterIds, int id) {
        Movie m = movieRepository.findById(id).get();
        m.getCharacters().clear();
        for (int cid: characterIds) {
            m.getCharacters().add(characterRepository.findById(cid).get());
        }
        movieRepository.save(m);
    }
}