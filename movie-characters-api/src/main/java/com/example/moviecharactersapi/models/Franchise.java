package com.example.moviecharactersapi.models;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
public class Franchise {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(nullable = false, length = 50)
    String name;
    @Column(length = 250)
    String description;
    @OneToMany(mappedBy = "franchise")
    Set<Movie> movies;

    public void addMovie(Movie movie) {
        this.movies.add(movie);
        movie.setFranchise(this);
    }

    public HashSet<Character> getCharacters() {
        HashSet<Character> characters = new HashSet<>();
        for (Movie m: movies) {
            characters.addAll(m.getCharacters());
        }
        return characters;
    }
}
