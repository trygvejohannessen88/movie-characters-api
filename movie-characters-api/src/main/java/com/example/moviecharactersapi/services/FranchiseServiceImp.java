package com.example.moviecharactersapi.services;

import com.example.moviecharactersapi.exceptions.ResourceNotFoundException;
import com.example.moviecharactersapi.models.Franchise;
import com.example.moviecharactersapi.repositories.FranchiseRepository;
import com.example.moviecharactersapi.repositories.MovieRepository;
import org.springframework.stereotype.Service;
import java.util.Collection;

@Service
public class FranchiseServiceImp implements FranchiseService {
    private final FranchiseRepository franchiseRepository;
    private final MovieRepository movieRepository;

    public FranchiseServiceImp(FranchiseRepository franchiseRepository, MovieRepository movieRepository) {
        this.franchiseRepository = franchiseRepository;
        this.movieRepository = movieRepository;
    }

    @Override
    public Franchise findById(Integer id) {
        return franchiseRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Franchise With the Id: " + id + " Could not be found"));
    }

    @Override
    public Collection<Franchise> findAll() {
        return franchiseRepository.findAll();
    }



    @Override
    public Franchise add(Franchise entity) {
        return franchiseRepository.save(entity);
    }

    @Override
    public Franchise update(Franchise entity) {
        return franchiseRepository.save(entity);
    }

    @Override
    public void deleteById(Integer integer) {
        Franchise f = franchiseRepository.findById(integer).get();
        f.getMovies().forEach(m -> m.setFranchise(null));
        franchiseRepository.deleteById(integer);
    }

    @Override
    public void delete(Franchise entity) {
        Franchise f = franchiseRepository.findById(entity.getId()).get();
        f.getMovies().forEach(m -> m.setFranchise(null));
        franchiseRepository.delete(entity);
    }

    @Override
    public Collection<Franchise> findAllByName(String name) {
        return franchiseRepository.findAllByName(name);
    }

    @Override
    public void updateMovies(Collection<Integer> movieIds, int id) {
        Franchise franchise = franchiseRepository.findById(id).get();
        franchise.getMovies().forEach(movie -> movie.setFranchise(null));
        for (int mid: movieIds) {
            franchise.addMovie(movieRepository.findById(mid).get());
        }
        franchiseRepository.save(franchise);
    }

}
