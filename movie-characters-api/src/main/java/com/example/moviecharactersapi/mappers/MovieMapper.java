package com.example.moviecharactersapi.mappers;

import com.example.moviecharactersapi.models.Character;
import com.example.moviecharactersapi.models.Franchise;
import com.example.moviecharactersapi.models.Movie;
import com.example.moviecharactersapi.models.dtos.MovieDTO;
import com.example.moviecharactersapi.services.CharacterService;
import com.example.moviecharactersapi.services.FranchiseService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class MovieMapper {
    @Autowired
    protected FranchiseService franchiseService;
    @Autowired
    protected CharacterService characterService;

    @Mapping(target = "franchise", source = "franchise.id")
    @Mapping(target = "characters", source = "characters", qualifiedByName = "charactersToIds")
    public abstract MovieDTO movieToMovieDto(Movie movie);

    public abstract Collection<MovieDTO> movieToMovieDto(Collection<Movie> movies);

    @Mapping(target = "franchise", source = "franchise", qualifiedByName = "franchiseIdToFranchise")
    @Mapping(target = "characters", source = "characters", qualifiedByName = "charactersIdsToCharacters")
    public abstract Movie movieDtoToMovie(MovieDTO dto);

    // Custom mappings
    @Named("franchiseIdToFranchise")
    Franchise mapIdToProject(int id) {
        return franchiseService.findById(id);
    }

    @Named("charactersIdsToCharacters")
    Set<Character> mapIdsToCharacters(Set<Integer> id) {
        return id.stream()
                .map( i -> characterService.findById(i))
                .collect(Collectors.toSet());
    }

    @Named("charactersToIds")
    Set<Integer> mapCharactersToIds(Set<Character> source) {
        if(source == null)
            return null;
        return source.stream()
                .map(s -> s.getId()).collect(Collectors.toSet());
    }

}
