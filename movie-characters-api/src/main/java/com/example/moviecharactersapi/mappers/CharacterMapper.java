package com.example.moviecharactersapi.mappers;

import com.example.moviecharactersapi.models.Character;
import com.example.moviecharactersapi.models.Movie;
import com.example.moviecharactersapi.models.dtos.CharacterDTO;
import com.example.moviecharactersapi.services.MovieService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;


    @Mapper(componentModel = "spring")
    public abstract class CharacterMapper {

        @Autowired
        protected MovieService movieService;

        @Mapping(target = "movies", source = "movies", qualifiedByName = "moviesToIds")
        public abstract CharacterDTO characterToCharacterDTO(Character character);


        public abstract Collection<CharacterDTO> characterToCharacterDTOS(Collection<Character> characters);



        @Mapping(target = "movies", source = "movies", qualifiedByName = "movieIdsToMovies")
        public abstract Character characterDtoToCharacter(CharacterDTO dto);

        // Custom mappings
        @Named("moviesToIds")
        Set<Integer> mapMoviesToIds(Set<Movie> movies) {
            if(movies == null) {
                return null;
            }
            return movies.stream().map(movie -> movie.getId()).collect(Collectors.toSet());
        }

        @Named("movieIdsToMovies")
        Set<Movie> mapMovieIdsToMovies(Set<Integer> movieIds) {
            if (movieIds == null) {
                return null;
            }
            return movieIds.stream().map(id -> movieService.findById(id))
                    .collect(Collectors.toSet());
        }


    }

