

# database setup
The database can be setup with the provided docker-compose-yaml,
which contains an PostgreSQL and PGAdmin image.

1. Terminal: navigate to folder of the docker-compose.yaml file
2. Run: docker-compose up -d
   * This downloads the images and runs them, postgres accessible from 5454 and PGAdmin from 5050
3. browser: go to http://localhost:5050/
4. login: admin@admin.com and password: root
5. on the server, right click register->server:
   * name: postgres
   * (connection tab) Host: postgres
   * (connection tab) Username: postgres
   * (connection tab) password: postgres
6. The image have already created the database: movie-characters-api