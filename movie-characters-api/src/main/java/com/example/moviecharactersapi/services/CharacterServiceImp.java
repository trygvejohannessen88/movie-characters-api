package com.example.moviecharactersapi.services;

import com.example.moviecharactersapi.exceptions.ResourceNotFoundException;
import com.example.moviecharactersapi.models.Character;
import com.example.moviecharactersapi.repositories.CharacterRepository;
import org.springframework.stereotype.Service;
import java.util.Collection;

@Service
public class CharacterServiceImp implements CharacterService {
    private final CharacterRepository characterRepository;

    public CharacterServiceImp(CharacterRepository characterRepository) {
        this.characterRepository = characterRepository;
    }

    @Override
    public Character findById(Integer id) {
        return characterRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Franchise With the Id: " + id + " Could not be found"));
    }

    @Override
    public Collection<Character> findAll() {
        return characterRepository.findAll();
    }

    @Override
    public Character add(Character entity) {
        return characterRepository.save(entity);
    }

    @Override
    public Character update(Character entity) {
        return characterRepository.save(entity);
    }

    @Override
    public void deleteById(Integer integer) {
        Character c = characterRepository.findById(integer).get();
        c.getMovies().forEach(m -> m.getCharacters().remove(c));
        characterRepository.deleteById(integer);
    }

    @Override
    public void delete(Character entity) {
        Character c = characterRepository.findById(entity.getId()).get();
        c.getMovies().forEach(m -> m.getCharacters().remove(c));
        characterRepository.delete(entity);
    }

    @Override
    public Collection<Character> findAllByName(String name) {
        return null;
    }
}
