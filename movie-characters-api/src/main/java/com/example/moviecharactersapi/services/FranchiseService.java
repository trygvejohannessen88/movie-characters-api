package com.example.moviecharactersapi.services;

import com.example.moviecharactersapi.models.Franchise;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public interface FranchiseService extends CrudService<Franchise,Integer> {

    Collection<Franchise> findAllByName(String name);

    void updateMovies(Collection<Integer> movieIds, int id);
}